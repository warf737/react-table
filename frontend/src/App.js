import React, { Component, Fragment } from 'react'
import { Table,  Pagination} from 'semantic-ui-react';
import load from './services/load.service';
import { smallDataURL, bigDataURL } from './env/consts';
import ActiveCard from './components/ActiveCard';

class App extends Component {

    state = {
        data: [],
        isLoading: false,
        limit: 10,
        offset: 0,
        activePage: 1,
        search: '',
        activeColumn: null,
        activeCard: null,
        direction: null,
        error: null,
    };

    // загрузка данных с сервера
    async loadData(mode) {
        this.setState({ isLoading: true, error: null });

        let url;
        if (mode === 'small') url = smallDataURL;
        if (mode === 'big')url = bigDataURL;

        try {

            load(url).then(cards => {
            const parsedData = JSON.parse(cards);
            const initData= parsedData.data;
            this.setState({data: initData});
                this.setState({ isLoading: false })
        })
        } catch (err) {
            this.setState({ error: err.message })
        }

    }

// сброс фильтрации
    reset() {
        this.setState({
            data: this.state.data,
            isLoading: false,
            limit: 15,
            offset: 0,
            activePage: 1,
            search: '',
            activeColumn: null,
            activeCard: null,
            sortDirection : null,
            error: null,
        })
    }

    //измененение текущей страницы отображения
    handlePageChange = (e, {activePage}) => {
        const { limit } = this.state;
        const diff = activePage - this.state.activePage;

        if (activePage > this.state.activePage) {
            this.setState(prevState => ({
                offset: prevState.offset + limit * diff,
                activePage: activePage
            }))
        } else {
            this.setState(prevState => ({
                offset: prevState.offset + limit * diff,
                activePage: activePage
            }))
        }
    };

    // установка значения для фильтрации
    handleInputChange = e =>
        this.setState({
            [e.target.name]: e.target.value,
            activeCard: null,
            offset: 0,
            activePage: 1
        });



    // сортировка
    handleSort = selectedColumn => () =>{
        const { activeColumn, sortDirection , data } = this.state;

        if (activeColumn !== selectedColumn) {
            this.setState({
                activeColumn: selectedColumn,
                data: data.sort((a, b) => {
                    if (a[selectedColumn] < b[selectedColumn]) return -1;
                    if (a[selectedColumn] > b[selectedColumn]) return 1;
                    return 0
                }),

                sortDirection : 'ascending',
                offset: 0,
                activePage: 1
            });
            return;
        }
        this.setState({
            data: data.reverse(),
            sortDirection: sortDirection  === 'ascending' ? 'descending' : 'ascending',
            offset: 0,
            activePage: 1
        })
    };

    // включаем отображение подробной информации по выбраной строке
    displayActiveCard = card => {
        this.setState({
            activeCard: card
        })
    };


    render() {
        const {
            data,
            isLoading,
            limit,
            offset,
            activePage,
            search,
            activeColumn,
            activeCard,
            sortDirection ,
            error,
        } = this.state;

        let filteredData = this.state.data.filter(elem => {
            return Object.values({artist: elem.artist, name: elem.name, type_line: elem.type_line}, )
                .some(value => {
                      return value.toString()
                            .toLowerCase()
                            .indexOf(search.toLowerCase()) !== -1
                    }
                )
            });

        return (
            <Fragment>

                {error && (
                    <div className="err-msg">
                        <h2>{error}</h2>
                        <p>Something went wrong while upload data</p>
                    </div>
                )}


                {data.length === 0 && !isLoading &&(
                <div>
                    <button className="btn" onClick={() => this.loadData('small')}>
                        <i className="fa fa-sort-alpha-asc"></i>Load small dataset
                    </button>
                    <button className="btn" onClick={() => this.loadData('big')}>
                        <i className="fa fa-sort-numeric-desc"></i>Load bigger dataset
                    </button>
                </div>
                )}

                {isLoading && (
                    <div className="spinner-border" role="status">
                        <span className="sr-only">Loading...</span>
                    </div>
                )}

                {data.length !==0 && (

                    <div className='app container-fluid'>

                        <div className ='row'>
                            <nav className="navbar navbar-light bg-light">
                                    <input
                                placeholder="Search card "
                                className='form-control'
                                name='search'
                                value={search}
                                onChange={this.handleInputChange}
                                />
                                <button className="btn btn-default" onClick={() => this.reset()}>
                                    <i className="fa fa-sort-alpha-asc"></i>Reset filter
                                </button>

                                </nav>

                        <div className="row">
                            <div className='col-md-9'>

                        <Table sortable className="table table-striped table-hover">

                            <Table.Header>
                                <Table.Row >
                                    <Table.HeaderCell
                                    sorted={activeColumn === 'name' ? sortDirection  : null}
                                    onClick={this.handleSort ('name')}
                                    >
                                        Name
                                    </Table.HeaderCell>

                                    <Table.HeaderCell
                                        sorted={activeColumn === 'cmc' ? sortDirection  : null}
                                        onClick={this.handleSort ('cmc')}
                                    >
                                        Mana cost
                                    </Table.HeaderCell>

                                    <Table.HeaderCell
                                        sorted={activeColumn === 'type_line' ? sortDirection  : null}
                                        onClick={this.handleSort ('type_line')}
                                    >
                                        Type
                                    </Table.HeaderCell>

                                    <Table.HeaderCell
                                        sorted={activeColumn === 'artist' ? sortDirection  : null}
                                        onClick={this.handleSort ('artist')}
                                    >
                                        Artist
                                    </Table.HeaderCell>

                                    <Table.HeaderCell
                                        sorted={activeColumn === 'oracle_text' ? sortDirection  : null}
                                        onClick={this.handleSort ('oracle_text')}
                                    >
                                        Oracle
                                    </Table.HeaderCell>
                                </Table.Row>
                            </Table.Header>

                            <Table.Body>
                                {filteredData.slice(offset, offset+limit).map( card => (


                                    <Table.Row
                                    key={card.id}
                                    onClick={() => this.displayActiveCard(card)}
                                    >
                                        <Table.Cell width={2}>{card.name}</Table.Cell>
                                        <Table.Cell width={2}>{card.mana_cost}</Table.Cell>
                                        <Table.Cell width={2}>{card.type_line}</Table.Cell>
                                        <Table.Cell width={2}>{card.artist}</Table.Cell>
                                        <Table.Cell>{card.oracle_text}</Table.Cell>
                                    </Table.Row>
                                ))}
                            </Table.Body>

                            <Table.Footer>
                                <Table.Row>
                                    <Table.HeaderCell colSpan='5'>
                                        <Pagination
                                        activePage={activePage}
                                        onPageChange={this.handlePageChange}
                                        totalPages={Math.ceil(filteredData.length / limit)}
                                        />
                                    </Table.HeaderCell>
                                </Table.Row>
                            </Table.Footer>
                        </Table>
                        </div>

                            {activeCard && <ActiveCard  {...activeCard} />}

                        </div>
                     </div>
                 </div>
               )}
            </Fragment>
        )
    }
}
export default App





