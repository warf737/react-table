import React from 'react';
import { Segment } from 'semantic-ui-react';

export default ({name, mana_cost, oracle_text, set_name, legalities, image_uris, card_faces, type_line}) => {

    let img;
    (!image_uris) ? img = card_faces[0].image_uris.normal : img = image_uris.normal;

    return (
        <Segment style={{ marginBottom: '3em' }} className='col-md-3 sticky-top'>
            <b>
             <img src={`${img}`} height='450px' className='center-block'/>
            </b><br />
                <p></p>

            Card: {' '} <b>{name}</b>
             <br />

            Mana cost: <b>{mana_cost}</b>
                <br />
            Type: <b>{type_line}</b>
                <br />
            Oracle: <b>{oracle_text}</b>
            <br />
            Set: <b>{set_name}</b>
            <br />
            Legality on standart: <b>{legalities.standard}</b>
            <br />
            Legality on modern: <b>{legalities.modern}</b>
            <br />
            Legality on commander: <b>{legalities.commander}</b>
            <br />
        </Segment>
    );
};



// {/*<div >*/}
// {/*    { /*<img src={`${card.image_uris.small}`} /> *!/*/}
//
// {/*    <div >*/}
// {/*        <h3>{card.name}</h3>*/}
// {/*        <table >*/}
// {/*            <tbody>*/}
// {/*            <tr>*/}
// {/*                <td>Mana cost</td>*/}
// {/*                <td>{card.mana_cost}</td>*/}
// {/*            </tr>*/}
// {/*            <tr>*/}
// {/*                <td>Type</td>*/}
// {/*                <td>{card.type_line}</td>*/}
// {/*            </tr>*/}
// {/*            <tr>*/}
// {/*                <td>Oracle</td>*/}
// {/*                <td>{card.oracle_text}</td>*/}
// {/*            </tr>*/}
// {/*            </tbody>*/}
// {/*        </table>*/}
// {/*    </div>*/}
// {/*/!*</div>Z*!/*/}